/**
 * 
 */
package co.gov.mineducacion.sigaa.web.ws.dominio;

/**
 * Campos definidos para dar respuesta generica, cada respuesta del ws debe tener
 * un objeto de este tipo
 * 
 * @author rlozada
 *
 */
public class GenericResponse {
	
	/**
	 * Codigo de respuesta definido
	 */
	private String codigoRespuesta;
	
	/**
	 * Mensaje asociado al codigo de respuesta
	 */
	private String mensajeRespuesta;

	/**
	 * @return the codigoRespuesta
	 */
	public String getCodigoRespuesta() {
		return codigoRespuesta;
	}

	/**
	 * @param codigoRespuesta the codigoRespuesta to set
	 */
	public void setCodigoRespuesta(String codigoRespuesta) {
		this.codigoRespuesta = codigoRespuesta;
	}

	/**
	 * @return the mensajeRespuesta
	 */
	public String getMensajeRespuesta() {
		return mensajeRespuesta;
	}

	/**
	 * @param mensajeRespuesta the mensajeRespuesta to set
	 */
	public void setMensajeRespuesta(String mensajeRespuesta) {
		this.mensajeRespuesta = mensajeRespuesta;
	}
	
	@Override
	public String toString()
	{
		StringBuilder sb = new StringBuilder();
		sb.append(ReportarActoRequest.class.getName());
		sb.append(" : con atributos: { CodigoRespuesta=");
		sb.append(getCodigoRespuesta());
		sb.append(" - MensajeRespuesta=");
		sb.append(getMensajeRespuesta());
		sb.append(" } ");
		return sb.toString();
	}
	

}
