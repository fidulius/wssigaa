package co.gov.mineducacion.sigaa.web.ws.business;

import javax.ejb.Stateless;

import org.apache.log4j.Logger;

import co.gov.mineducacion.sigaa.web.ws.dominio.GenericResponse;
import co.gov.mineducacion.sigaa.web.ws.dominio.MensajesWS;
import co.gov.mineducacion.sigaa.web.ws.dominio.ReportarActoRequest;

@Stateless(name="BsNotificacionesSigaa")
public class BsNotificacionesSigaa implements IBsNotificacionesSigaa {
	
	private static Logger logger = Logger.getLogger(BsNotificacionesSigaa.class);

	@Override
	public GenericResponse insertarActo(ReportarActoRequest acto) {
		logger.info("Entrando a insertarActo con parametros: " + acto);
		GenericResponse result = new GenericResponse();
		try 
		{
			logger.info("Almacenando objeto en la base de datos");
			result.setCodigoRespuesta(MensajesWS.CODIGO_OK);
			result.setMensajeRespuesta(MensajesWS.MENSAJE_OK);
		}
		catch(Exception e)
		{
			logger.error("Error al guardar la solicitud ", e);
			result.setCodigoRespuesta(MensajesWS.COD_ERROR_PORTAFIRMA);
			result.setMensajeRespuesta(MensajesWS.MSJ_ERROR_NO_CONTROLADO);
		}
		
		logger.info("Saliendo de insertarActo con respuesta: " + result.toString());
		return result;
	}

}
