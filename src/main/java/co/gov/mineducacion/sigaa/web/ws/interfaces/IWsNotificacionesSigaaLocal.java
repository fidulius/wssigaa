/**
 * 
 */
package co.gov.mineducacion.sigaa.web.ws.interfaces;

import javax.ejb.Local;
import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebResult;

import co.gov.mineducacion.sigaa.web.ws.dominio.GenericResponse;
import co.gov.mineducacion.sigaa.web.ws.dominio.ReportarActoRequest;

/**
 * @author rlozada
 *
 */
@Local
@WebService
public interface IWsNotificacionesSigaaLocal {
	
	@WebMethod(operationName="reportarActo")
	@WebResult(name="GenericResponse")
	public GenericResponse reportarActo(ReportarActoRequest acto);
}
