package co.gov.mineducacion.sigaa.web.ws.dominio;

import java.util.Date;

public class ActosAdministrativosWSDTO {

	
	private Long secActoAdmin;
	private String numeroRadicadoActo;
	private Date fechaRadicado;
	private String aplicaRecurso;
	private String compulsarCopia;
	private String tipoDecision;
	private String estadoProcesoGnal;
	private Long consDependencia;
	private String tipoTramite;
	private String proceso;
	private Long consTipoResolucion;
	private String estadoRegistro;
	private Date fechaCreacion;
	private String usuarioCreacion;
	private String descripcion;
	private Long idProcesoWorkflow;
	private Long idProcesoNotificacionWorkflow;
	private String numeroSolicitudProceso;
	private String programa;
	private Long secActoAdminPadre;
	private String observacion;
	private Long numeroResolucion;
	private Date autoFecha;
	private Long secUsuario;
	private String rutaActoAdmin;
	private Date fechaModificacion;
	private String usuarioModificacion;
	private Long numeroPaginas;
	private String tipoActo;

	public Long getSecActoAdmin() {
		return this.secActoAdmin;
	}

	public void setSecActoAdmin(Long secActoAdmin) {
		this.secActoAdmin = secActoAdmin;
	}

	public String getNumeroRadicadoActo() {
		return this.numeroRadicadoActo;
	}

	public void setNumeroRadicadoActo(String numeroRadicadoActo) {
		this.numeroRadicadoActo = numeroRadicadoActo;
	}

	public Date getFechaRadicado() {
		return this.fechaRadicado;
	}

	public void setFechaRadicado(Date fechaRadicado) {
		this.fechaRadicado = fechaRadicado;
	}

	public String getAplicaRecurso() {
		return this.aplicaRecurso;
	}

	public void setAplicaRecurso(String aplicaRecurso) {
		this.aplicaRecurso = aplicaRecurso;
	}

	public String getCompulsarCopia() {
		return this.compulsarCopia;
	}

	public void setCompulsarCopia(String compulsarCopia) {
		this.compulsarCopia = compulsarCopia;
	}

	public String getTipoDecision() {
		return this.tipoDecision;
	}

	public void setTipoDecision(String tipoDecision) {
		this.tipoDecision = tipoDecision;
	}

	public String getEstadoProcesoGnal() {
		return this.estadoProcesoGnal;
	}

	public void setEstadoProcesoGnal(String estadoProcesoGnal) {
		this.estadoProcesoGnal = estadoProcesoGnal;
	}

	public Long getConsDependencia() {
		return this.consDependencia;
	}

	public void setConsDependencia(Long consDependencia) {
		this.consDependencia = consDependencia;
	}

	public String getTipoTramite() {
		return this.tipoTramite;
	}

	public void setTipoTramite(String tipoTramite) {
		this.tipoTramite = tipoTramite;
	}

	public String getProceso() {
		return this.proceso;
	}

	public void setProceso(String proceso) {
		this.proceso = proceso;
	}

	public Long getConsTipoResolucion() {
		return this.consTipoResolucion;
	}

	public void setConsTipoResolucion(Long consTipoResolucion) {
		this.consTipoResolucion = consTipoResolucion;
	}

	public String getEstadoRegistro() {
		return this.estadoRegistro;
	}

	public void setEstadoRegistro(String estadoRegistro) {
		this.estadoRegistro = estadoRegistro;
	}

	public Date getFechaCreacion() {
		return this.fechaCreacion;
	}

	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	public String getUsuarioCreacion() {
		return this.usuarioCreacion;
	}

	public void setUsuarioCreacion(String usuarioCreacion) {
		this.usuarioCreacion = usuarioCreacion;
	}

	public String getDescripcion() {
		return this.descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Long getIdProcesoWorkflow() {
		return this.idProcesoWorkflow;
	}

	public void setIdProcesoWorkflow(Long idProcesoWorkflow) {
		this.idProcesoWorkflow = idProcesoWorkflow;
	}

	public Long getIdProcesoNotificacionWorkflow() {
		return idProcesoNotificacionWorkflow;
	}

	public void setIdProcesoNotificacionWorkflow(Long idProcesoNotificacionWorkflow) {
		this.idProcesoNotificacionWorkflow = idProcesoNotificacionWorkflow;
	}

	public String getNumeroSolicitudProceso() {
		return this.numeroSolicitudProceso;
	}

	public void setNumeroSolicitudProceso(String numeroSolicitudProceso) {
		this.numeroSolicitudProceso = numeroSolicitudProceso;
	}

	public String getPrograma() {
		return this.programa;
	}

	public void setPrograma(String programa) {
		this.programa = programa;
	}

	public Long getSecActoAdminPadre() {
		return this.secActoAdminPadre;
	}

	public void setSecActoAdminPadre(Long secActoAdminPadre) {
		this.secActoAdminPadre = secActoAdminPadre;
	}

	public String getObservacion() {
		return this.observacion;
	}

	public void setObservacion(String observacion) {
		this.observacion = observacion;
	}

	public Long getNumeroResolucion() {
		return this.numeroResolucion;
	}

	public void setNumeroResolucion(Long numeroResolucion) {
		this.numeroResolucion = numeroResolucion;
	}

	public Date getAutoFecha() {
		return this.autoFecha;
	}

	public void setAutoFecha(Date autoFecha) {
		this.autoFecha = autoFecha;
	}

	public Long getSecUsuario() {
		return this.secUsuario;
	}

	public void setSecUsuario(Long secUsuario) {
		this.secUsuario = secUsuario;
	}

	public String getRutaActoAdmin() {
		return this.rutaActoAdmin;
	}

	public void setRutaActoAdmin(String rutaActoAdmin) {
		this.rutaActoAdmin = rutaActoAdmin;
	}

	public Date getFechaModificacion() {
		return this.fechaModificacion;
	}

	public void setFechaModificacion(Date fechaModificacion) {
		this.fechaModificacion = fechaModificacion;
	}

	public String getUsuarioModificacion() {
		return this.usuarioModificacion;
	}

	public void setUsuarioModificacion(String usuarioModificacion) {
		this.usuarioModificacion = usuarioModificacion;
	}


	public Long getNumeroPaginas() {
		return numeroPaginas;
	}

	public void setNumeroPaginas(Long numeroPaginas) {
		this.numeroPaginas = numeroPaginas;
	}

	/**
	 * @return the tipoActo
	 */
	public String getTipoActo() {
		return tipoActo;
	}

	/**
	 * @param tipoActo the tipoActo to set
	 */
	public void setTipoActo(String tipoActo) {
		this.tipoActo = tipoActo;
	}
	
	@Override
	public String toString()
	{
		StringBuilder sb = new StringBuilder();
		sb.append(this.getClass().getName());
		sb.append(" : con atributos: { NumeroRadicadoActo=");
		sb.append(getNumeroRadicadoActo());
		sb.append(" - numeroResolucion=");
		sb.append(getNumeroResolucion());
		sb.append(" }");
		return sb.toString();
	}

}
